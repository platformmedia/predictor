from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^predict/$', views.prediction_view, name='prediction_view'),
]

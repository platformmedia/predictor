from django.contrib import admin
from .models import TrainingData, TestingData


class TrainingDataAdmin(admin.ModelAdmin):
    list_display = ['title', 'negative_feedback', 'ctr', 'sentiment_score']
    list_filter = ['sentiment_score', 'manually_added']
    search_fields = ['title']


class TestingDataAdmin(admin.ModelAdmin):
    list_display = ['title', 'negative_feedback', 'predicted_negative_feedback', 'error_negative_feedback', 'ctr', 'predicted_ctr', 'error_ctr', 'sentiment_score', 'predicted_sentiment_score']
    list_filter = ['sentiment_score', 'predicted_sentiment_score']
    search_fields = ['title', 'description']

admin.site.register(TrainingData, TrainingDataAdmin)
admin.site.register(TestingData, TestingDataAdmin)

from __future__ import unicode_literals

from django.db import models


class TrainingData(models.Model):

    title = models.CharField(max_length=2000)
    description = models.TextField()

    negative_feedback = models.IntegerField(null=True, blank=True)
    ctr = models.IntegerField(null=True, blank=True)
    post_type = models.CharField(max_length=100, null=True, blank=True)
    manually_added = models.BooleanField(default=False)

    NEUTRAL = "NEUTRAL"
    POSITIVE = "POSITIVE"
    NEGATIVE = "NEGATIVE"
    sentiment_choices = [
        (0, NEUTRAL),
        (1, POSITIVE),
        (-1, NEGATIVE)
    ]

    sentiment_score = models.IntegerField(choices=sentiment_choices, default=0)

    class Meta:
        verbose_name = "TrainingData"
        verbose_name_plural = "TrainingData"

    def __str__(self):
        return self.title


class TestingData(models.Model):

    title = models.CharField(max_length=2000)
    description = models.TextField()

    negative_feedback = models.IntegerField()
    predicted_negative_feedback = models.IntegerField(null=True, blank=True)
    error_negative_feedback = models.IntegerField(null=True, blank=True)

    ctr = models.IntegerField()
    predicted_ctr = models.IntegerField(null=True, blank=True)
    error_ctr = models.IntegerField(null=True, blank=True)

    NEUTRAL = "NEUTRAL"
    POSITIVE = "POSITIVE"
    NEGATIVE = "NEGATIVE"
    sentiment_choices = [
        (0, NEUTRAL),
        (1, POSITIVE),
        (-1, NEGATIVE)
    ]

    sentiment_score = models.IntegerField(choices=sentiment_choices, default=0)
    predicted_sentiment_score = models.IntegerField(choices=sentiment_choices, default=0)

    class Meta:
        verbose_name = "TestingData"
        verbose_name_plural = "TestingData"

    def __str__(self):
        return self.title


class Profanity(models.Model):

    name = models.CharField(max_length=50, db_index=True)

    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

import math

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm

from .models import TrainingData

sentiment_choices_dict = {0: 'NEUTRAL', 1: 'POSITIVE', -1: 'NEGATIVE'}


def get_prediction_for_text(text):
    data = TrainingData.objects.all().values_list('description', flat=True)
    target = TrainingData.objects.all().values_list('sentiment_score', flat=True)

    regression_pipeline = Pipeline([
        ('count_vectorizer', CountVectorizer(ngram_range=(1, 3))),
        # ('tfidf',              TfidfTransformer()),
        ('classification', MultinomialNB())
    ])

    print text
    regression_pipeline.fit(data, target)
    result = regression_pipeline.predict([text])
    sentiment_score = int(math.ceil(result[0]))
    print sentiment_score
    return sentiment_choices_dict[sentiment_score]

from django.core.management.base import BaseCommand, CommandError

import json
import requests
from bs4 import BeautifulSoup


from predictor.apps.training.models import *


class Command(BaseCommand):
    help = 'Utility command to predict post ctr\
            ./manage.py parse_jokes'

    def handle(self, *args, **options):
        for i in range(2, 20):
            racist_jokes_endpoint = 'http://www.laughfactory.com/jokes/racist-jokes/{}'.format(i)
            resp = requests.get(racist_jokes_endpoint)
            if resp.status_code == 200:
                soup = BeautifulSoup(resp.text, 'html.parser')
                joke_texts = soup.find_all('div', {'class': 'joke-text'})
                for joke in joke_texts:
                    k = {}
                    k['title'] = 'Manual title'
                    k['post_type'] = 'Link'
                    k['description'] = joke.get_text().strip()
                    k['sentiment_score'] = -1
                    k['manually_added'] = True
                    print joke

                    trainingset, created = TrainingData.objects.get_or_create(**k)

        for i in range(2, 9):
            sexist_jokes_endpoint = 'http://www.laughfactory.com/jokes/sexist-jokes/{}'.format(i)
            print sexist_jokes_endpoint
            resp = requests.get(sexist_jokes_endpoint)
            if resp.status_code == 200:
                soup = BeautifulSoup(resp.text, 'html.parser')
                joke_texts = soup.find_all('div', {'class': 'joke-text'})
                for joke in joke_texts:
                    k = {}
                    k['title'] = 'Manual title'
                    k['post_type'] = 'Link'
                    k['description'] = joke.get_text().strip()
                    k['sentiment_score'] = -1
                    k['manually_added'] = True
                    # print joke

                    trainingset, created = TrainingData.objects.get_or_create(**k)

        for i in range(2, 9):
            sexist_jokes_endpoint = 'http://www.laughfactory.com/jokes/sex-jokes/{}'.format(i)
            print sexist_jokes_endpoint
            resp = requests.get(sexist_jokes_endpoint)
            if resp.status_code == 200:
                soup = BeautifulSoup(resp.text, 'html.parser')
                joke_texts = soup.find_all('div', {'class': 'joke-text'})
                for joke in joke_texts:
                    k = {}
                    k['title'] = 'Manual title'
                    k['post_type'] = 'Link'
                    k['description'] = joke.get_text().strip()
                    k['sentiment_score'] = -1
                    k['manually_added'] = True
                    # print joke

                    trainingset, created = TrainingData.objects.get_or_create(**k)

from django.core.management.base import BaseCommand, CommandError

import csv
from predictor.apps.training.models import *

import pandas as pd
import numpy as np
import math

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm

class Command(BaseCommand):
    help = 'Utility command to predict negative_feedback\
            ./manage.py predicted_negative_feedback'

    def handle(self, *args, **options):

        # sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran
        # now take data and target from predictor model
        data = TrainingData.objects.all().values_list('description', flat=True)
        target = TrainingData.objects.all().values_list('sentiment_score', flat=True)

        regression_pipeline = Pipeline([
            ('count_vectorizer',   CountVectorizer(ngram_range=(1,  3))),
            # ('tfidf',              TfidfTransformer()),
            ('classification',     MultinomialNB())
        ])

        regression_pipeline.fit(data, target)

        test_descs = TestingData.objects.all().values_list('description', flat=True)
        test_target = TestingData.objects.all().values_list('sentiment_score', flat=True)

        for item in TestingData.objects.all():
            result = regression_pipeline.predict([item.description])
            actual_sentiment = item.sentiment_score
            print result[0]
            predicted_sentiment_score = int(math.ceil(result[0]))
            item.predicted_sentiment_score = predicted_sentiment_score

            actual_negative_feedback = item.negative_feedback
            predicted_negative_feedback = int(math.ceil(result[0]))
            error_negative_feedback = actual_negative_feedback - predicted_negative_feedback
            item.predicted_negative_feedback = predicted_negative_feedback
            item.error_negative_feedback = error_negative_feedback
            # error_sentiment = actual_sentiment - predicted_sentiment
            # item.error_sentiment = error_sentiment
            item.save()

        # test_target = TestingData.objects.all().values_list('negative_feedback', flat=True)

        # for item in TestingData.objects.all():
        #     result = regression_pipeline.predict([item.description])
        #     actual_negative_feedback = item.negative_feedback
        #     predicted_negative_feedback = int(math.ceil(result[0]))
        #     error_negative_feedback = actual_negative_feedback - predicted_negative_feedback
        #     item.predicted_negative_feedback = predicted_negative_feedback
        #     item.error_negative_feedback = error_negative_feedback
        #     item.save() 




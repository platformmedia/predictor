from django.core.management.base import BaseCommand, CommandError

import csv
from predictor.apps.training.models import *

import pandas as pd
import numpy as np
import math

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn import svm

class Command(BaseCommand):
    help = 'Utility command to predict post ctr\
            ./manage.py predicted_ctr'

    def handle(self, *args, **options):

        # sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran
        # now take data and target from predictor model
        data = TrainingData.objects.all().values_list('description', flat=True)
        target = TrainingData.objects.all().values_list('ctr', flat=True)

        regression_pipeline = Pipeline([            
            ('count_vectorizer',   CountVectorizer(ngram_range=(1,  2))),
            ('regression',         svm.LinearSVR())
        ])

        regression_pipeline.fit(data, target)

        test_descs = TestingData.objects.all().values_list('description', flat=True)
        test_target = TestingData.objects.all().values_list('ctr', flat=True)

        for item in TestingData.objects.all():
            result = regression_pipeline.predict([item.description])
            actual_ctr = item.ctr
            predicted_ctr = int(math.ceil(result[0]))
            error_ctr = actual_ctr - predicted_ctr
            item.predicted_ctr = predicted_ctr
            item.error_ctr = error_ctr
            item.save() 




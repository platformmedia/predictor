from django import forms


class PredictionForm(forms.Form):
    """Form for prediction view"""
    text = forms.CharField(label='enter text here', widget=forms.Textarea(attrs={'cols': '100', 'rows': '10'}))

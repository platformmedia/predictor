from django.shortcuts import render

from .forms import PredictionForm
from .predictor import get_prediction_for_text


def prediction_view(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PredictionForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            text = form.cleaned_data['text']
            sentiment = get_prediction_for_text(text)
            result_string = 'Here is the result for this post : '
            return render(request, 'index.html', locals())

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PredictionForm()
    return render(request, 'index.html', locals())

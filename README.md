# predictor

* It predicts a sentiment score for any facebook post. Result set is positive, negative or neutral.
 
* Training data is based on some facebook posts and their performance is enumarated to sentiment scores.
 
* Uses CountVectorizer and Multinomial NB classifier to predict.

* To test you can use http://predictor.topday.com/predict
 
#### Steps to setup and run project
Make sure you have `python-setuptools, virtualenv & pip` installed.  
For installing system dependencies needed for scikit learn.
sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran

```
virtualenv ../env_predictor
source ../env_predictor/bin/activate
pip install -r requirements.txt
bower install --allow-root
python manage.py collectstatic
python manage.py migrate
python manage.py loaddata predictor/apps/training/fixtures/initial_data.json
python manage.py createsuperuser
python manage.py runserver
```


Visit http://localhost:8000/predict